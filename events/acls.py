import json
import requests

from .keys import OPEN_WEATHER_API_KEY, PEXELS_API_KEY


def get_weather_data(city, state):
    params = {
        'query': f"{city},{state}, US",
        'per_page': 1,
        'appid': OPEN_WEATHER_API_KEY
    }
    url = "http://api.openweathermap.org/geo/1.0/direct"
    response = requests.get(url, params=params)
    content = json.loads(response.content)

    try:
        latitude = content[0]['lat']
        longtitude = content[0]['lon']
    except(KeyError, IndexError):
        return None

    params = {
        'lat': latitude,
        'lon': longtitude,
        'appid': OPEN_WEATHER_API_KEY,
        'units': 'imperial',
    }
    url = 'https://api.openweathermap.org/data/2.5/weather'
    response = requests.get(url, params=params)
    content = json.loads(response.content)

    try:
        return {
            'description': content['weather'][0]['description'][0],
            'temp': content['main']['temp'],
        }
    except (KeyError, IndexError):
        return None


# picture_url = content['photos'][0]['src']['original']
def get_photo(city, state):
    headers = {
        'Authorization': PEXELS_API_KEY,
    }
    params = {
        'query': f"{city},{state}",
        'per_page': 1,
    }
    url = "https://api.pexels.com/v1/search"
    response = requests.get(url, params=params, headers=headers)
    content = json.loads(response.content)
    try:
        return {"picture_url": content["photos"][0]["src"]["original"]}
    except(Exception):
        return {"picture_url": None}
