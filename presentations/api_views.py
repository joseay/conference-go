from django.http import JsonResponse
from .models import Presentation
from common.json import ModelEncoder
from events.api_views import ConferenceListEncoder, Conference
from django.views.decorators.http import require_http_methods
import json


class PresentationListEncoder(ModelEncoder):
    model = Presentation
    properties = [
        'title',
        'status'
    ]

    def get_extra_data(self, o):
        return {
            'status': o.status.name,
            'href': o.get_api_url(),
        }


class PresentationDetailEncoder(ModelEncoder):
    model = Presentation
    properties = [
        "presenter_name",
        "company_name",
        "presenter_email",
        "title",
        "synopsis",
        "created",
        "conference",
        "status",
    ]
    encoders = {
        'conference': ConferenceListEncoder(),
    }
    
    def get_extra_data(self, o):
        return {
            'status': o.status.name,
        }


@require_http_methods(['POST', 'GET'])
def api_list_presentations(request, conference_id):
    if request.method == 'GET':
        presentation = Presentation.objects.filter(conference=conference_id)
        return JsonResponse(
            {'presentation': presentation},
            encoder=PresentationListEncoder,
            safe=False,
        )
    else:
        content = json.loads(request.body)
        try:
            conference = Conference.objects.get(id=conference_id)
            content['conference'] = conference
        except Conference.DoesNotExist:
            return JsonResponse(
                {'message': 'Invalid conference id'},
                status=400,
            )
        presentation = Presentation.create(**content)
        return JsonResponse(
            presentation,
            encoder=PresentationDetailEncoder,
            safe=False,
            )


@require_http_methods(['DELETE', 'GET', 'PUT'])
def api_show_presentation(request, id):
    
    if request.method == 'GET':
        presentation = Presentation.objects.get(id=id)
        return JsonResponse(
            presentation,
            encoder=PresentationDetailEncoder,
            safe=False,
            )
    elif request.method == 'DELETE':
        count, _ = Presentation.objects.filter(id=id).delete()
        return JsonResponse(
            {'delete': count > 0})
    else:
        content = json.loads(request.body)
        Presentation.objects.filter(id=id).update(**content)
        presentation = Presentation.objects.get(id=id)
        return JsonResponse(
            presentation,
            encoder=PresentationDetailEncoder,
            safe=False,
        )
